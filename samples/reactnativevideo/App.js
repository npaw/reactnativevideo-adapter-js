/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import Youbora from 'youboralib';
import Video from 'react-native-video';
import ReactNativeVideoAdapter from 'youbora-adapter-reactnativevideo';

window.plugin = new Youbora.Plugin({
  accountCode: 'powerdev',
  'content.title': 'react native',
  'app.https': true,
});

const Section = ({children, title}): Node => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

class App extends React.Component {
  render() {
    return (
      <Video
        source={{
          //uri: 'http://cdn3.viblast.com/streams/hls/airshow/playlist.m3u8',
          uri: 'https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8',
          //uri: 'https://sec.ch9.ms/ch9/6b3d/57930795-7f62-4218-8e18-888623426b3d/Goodfellow_mid.mp4',
        }}
        style={{width: 300, height: 300}}
        controls={true}
        ref={ref => {
          if (ref) {
            this.ybAdapter = new ReactNativeVideoAdapter(ref);
            window.plugin.setAdapter(this.ybAdapter);
          }
        }}
        onLoad={e => this.ybAdapter.onLoad(e)}
        onProgress={e => this.ybAdapter.onProgress(e)}
        onPlaybackRateChange={e => this.ybAdapter.onPlaybackRateChange(e)}
        onLoadStart={e => this.ybAdapter.onLoadStart(e)}
        onError={e => this.ybAdapter.onError(e)}
        onEnd={e => this.ybAdapter.onEnd(e)}
      />
    );
  }
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
