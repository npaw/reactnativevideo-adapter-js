var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.ReactNativeVideo = youbora.Adapter.extend({
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  getPlayhead: function () {
    return this.playhead
  },

  getIsLive: function() {
    return !this.duration
  },

  getDuration: function () {
    return this.duration
  },

  getResource: function () {
    return this.player.props.source.uri
  },

  getRendition: function () {
    if (this.width && this.height) {
      return youbora.Util.buildRenditionString(this.width, this.height);
    }
    return null
  },

  getPlayerName: function () {
    return 'React Native Video'
  },

  registerListeners: function () {
    this.monitorPlayhead(true, true, 1000)
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    if (this.monitor) this.monitor.stop()
  },

  onLoad: function (e) {
    var size = e.naturalSize
    if (size) {
      this.width = Math.round(size.width)
      this.height = Math.round(size.height)
    }
    this.duration = e.duration
    this.fireStart()
  },

  onProgress: function (e) {
    this.playhead = e.currentTime
    this.fireStart()
    if (this.playhead > 0.5 && !this.flags.isJoined) {
      this.fireJoin()
      this.monitor.skipNextTick()
    }
  },

  onPlaybackRateChange: function (e) {
    this.fireStart()
    this.playrate = e.playbackRate
    this.playrate ? this.fireResume() : this.firePause()
  },

  onLoadStart: function (e) {
    this.fireStart()
    if (!this.flags.isJoined){
      this.fireJoin()
      this.monitor.skipNextTick()
    }
    this.fireResume()
    this.fireSeekEnd()
  },

  onError: function (e) {
    this.fireError(e.error.code, e.error.localizedDescription)
  },

  onEnd: function (e) {
    this.fireStop()
  }
})

module.exports = youbora.adapters.ReactNativeVideo
